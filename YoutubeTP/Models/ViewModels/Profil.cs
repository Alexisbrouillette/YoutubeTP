﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using static Youtube.Models.DataModels.Membres;

namespace YoutubeTP.Models.ViewModels
{
    [CustomValidation(typeof(Profil), "Valider1")]
    public class Profil
    {
        [Required]
        [DisplayName("Nom")]
        public string Utilisateur { get; set; }
        [Required]
        [DisplayName("Mot de passe:")]
        [DataType(DataType.Password)]
        public string MotDePasse { get; set; }
        [DisplayName("Vérifier votre mot de passe:")]
        [Required]
        [DataType(DataType.Password)]
        [Compare("MotDePasse")]
        public string VerifierMotDePasse { get; set; }
        [DisplayName("Courriel")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Courriel { get; set; }
        [DisplayName("Confirmer votre courriel")]
        [Compare("Courriel")]
        [Required]
        [DataType(DataType.EmailAddress)]
        public string CourrielConfirmation { get; set; }
        [Required, EnumDataType(typeof(Sexes))]
        public Sexes Sexe { get; set; }
        [Required, CustomDate, DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd MM yyyy}")]
        [DisplayName("Date de naissance")]
        public DateTime DateNaissance { get; set; }
        [Required]
        [DisplayName("Êtes-vous sur de vos changements ?")]
        public bool IAgree { get; set; }
        public static ValidationResult Valider1(Profil p)
        {
            if (!p.IAgree)
            {
                return new ValidationResult("Clique oui.", new[] { "IAgree" })
               ;
            }
            else return ValidationResult.Success;
        }

    }
    

}
   