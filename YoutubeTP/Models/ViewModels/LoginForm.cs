﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace YoutubeTP.Models.ViewModels
{
    public class LoginForm
    {
        [Required]
        [DisplayName("Nom")]
        public string NomUtilisateur { get; set; }
        [DisplayName("Mot de passe:")]
        [Required]
        [DataType(DataType.Password)]
        public string MotDePasse { get; set; }
       
    }
}