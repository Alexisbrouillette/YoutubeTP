﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Youtube.Models.DataModels
{
    [Table("Membres")]
    public class Membres
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PKMembreId { get; set; }
        [Index(IsUnique = true), Required, RegularExpression("[A-Za-z][A-Za-z0-9_]{4,20}"), MaxLength(20)]
        [DisplayName("Nom utilisateur")]
        public String NomUtilisateur { get; set; }
        [Required, MaxLength(80), MinLength(8)]
        [DisplayName("Mot de passe")]
        public String HashMotDePasse { get; set; }
        [Required, CustomDate, DataType(DataType.Date)]
        public DateTime DateNaissance { get; set; }

        public enum Sexes {
            [Display(Name = "Masculin")]
            homme,
            [Display(Name = "Féminin")]
            femme,
            [Display(Name = "Autre")]
            autre,
        }
        
        [Required, EnumDataType(typeof(Sexes))]
        public Sexes Sexe {get; set;}

        [Index(IsUnique = true), Required, DataType(DataType.EmailAddress), MaxLength(30)]
       // [DisplayName("Courriel")]
        public String Courriel { get; set; }

        [InverseProperty("Membres")]
        public virtual List<Chaines> Chaines { get; set;}

        public virtual List<Video> HistoriqueVideos { get; set; }

        
    }

    //L'age du user ne peut être plus haut que 120 ans.
    public class CustomDateAttribute : RangeAttribute
    {
        public CustomDateAttribute()
          : base(typeof(DateTime),
                  DateTime.Now.AddYears(-120).ToShortDateString(),
                  DateTime.Now.ToShortDateString())
        { }
    }

}