﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Youtube.Models.DataModels
{
    public enum Categories
    {
        tout,
        [Display(Name = "Jeux-vidéo")]
        jeux,
        [Display(Name = "Sport")]
        sport,
        [Display(Name = "Cuisine")]
        cuisine,
        [Display(Name = "Art")]
        art,
        [Display(Name = "Comédie")]
        comedie,
        [Display(Name = "Musique")]
        musique,
    }

    [Table("Video")]
    public class Video
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PKVideoId { get; set; }

        [Display(Name = "Titre de la vidéo")]
        [Required, MaxLength(50), MinLength(3)]
        public String titreVideo { get; set; }

        [Display(Name = "Nombre visionnement")]
        [Range(0, float.MaxValue)]
        public float CompteurVue { get; set; }

        [Display(Name = "Date de publication")]
        [DataType(DataType.Date)]
        public DateTime DateParution { get; set; }

        [Display(Name = "Catégorie")]
        [Required, EnumDataType(typeof(Categories))]
        public Categories Categorie { get; set; }
        [MaxLength(300)]
        public String Description { get; set; }

        [Display(Name = "Tags")]
        [MaxLength(120)]
        public String tags { get; set; }
        [ForeignKey("Chaines")]
        public int FKChainesId { get; set; }
        [InverseProperty("VideosChaine")]
        [ForeignKey("FKChainesId")]
        public virtual Chaines Chaines { get; set; }

        //image
        [Display(Name = "Miniature")]
        public String srcImg { get; set; }

        public string srcVid { get; set; }


    }
}