﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;

namespace Youtube.Models.DataModels
{
    public class YoutubeDB : DbContext
    {
        public YoutubeDB() : base("ChaineConnexion")
        {
            Database.SetInitializer(new YoutInitializer());

        }
        public DbSet<Membres> Membres{get;set;}
        public DbSet<Chaines> Chaines { get; set; }

        public DbSet<Video> Video { get; set; }

       
    }
    
    public class YoutInitializer : DropCreateDatabaseIfModelChanges<YoutubeDB>
    {
        public YoutInitializer() : base() { }

        public override void InitializeDatabase(YoutubeDB context)
        {
            base.InitializeDatabase(context);
            

        }
        

        protected override void Seed(YoutubeDB context)
        {
            base.Seed(context);
            List<Membres> UserYoutube = new List<Membres>();
            
            Membres m1 = new Membres
            {
                NomUtilisateur = "m1UserName",
                HashMotDePasse = EncrypteMotDePasse("m1MotDePasse"),
                DateNaissance = Convert.ToDateTime("12/09/2000"),
                Sexe = Membres.Sexes.homme,
                Courriel = "m1@gmail.com",
                Chaines = null,
                HistoriqueVideos = null,
            };
            UserYoutube.Add(m1);
            context.Membres.Add(m1);

            Membres m2 = new Membres
            {
                NomUtilisateur = "m2UserName",
                HashMotDePasse = EncrypteMotDePasse("m2MotDePasse"),
                DateNaissance = Convert.ToDateTime("12/10/2000"),
                Sexe = Membres.Sexes.femme,
                Courriel = "m2@gmail.com",
                Chaines = null,
                HistoriqueVideos = null,
            };
            UserYoutube.Add(m2);
            context.Membres.Add(m2);

            Membres m3 = new Membres
            {
                NomUtilisateur = "m3UserName",
                HashMotDePasse = EncrypteMotDePasse("m3MotDePasse"),
                DateNaissance = Convert.ToDateTime("12/11/2000"),
                Sexe = Membres.Sexes.autre,
                Courriel = "m3@gmail.com",
                Chaines = null,
                HistoriqueVideos = null,
            };
            UserYoutube.Add(m3);
            context.Membres.Add(m3);

            Membres m4 = new Membres
            {
                NomUtilisateur = "m4UserName",
                HashMotDePasse = EncrypteMotDePasse("m4MotDePasse"),
                DateNaissance = Convert.ToDateTime("12/12/2000"),
                Sexe = Membres.Sexes.homme,
                Courriel = "m4@gmail.com",
                Chaines = null,
                HistoriqueVideos = null,
            };
            UserYoutube.Add(m4);
            context.Membres.Add(m4);

            Membres m5 = new Membres
            {
                NomUtilisateur = "m5UserName",
                HashMotDePasse = EncrypteMotDePasse("m5MotDePasse"),
                DateNaissance = Convert.ToDateTime("12/01/2000"),
                Sexe = Membres.Sexes.femme,
                Courriel = "m5@gmail.com",
                Chaines = null,
                HistoriqueVideos = null,
            };
            UserYoutube.Add(m5);
            context.Membres.Add(m5);
            //-----------------------------------------------------------------

            Chaines c1 = new Chaines
            {
                NomChaine = "c1Nom",
                Description = "Chaine de c1",
                Categorie = Categories.jeux,
                tags = "",
                Membres= m1,
                VideosChaine = null
            };
            context.Chaines.Add(c1);
            

            Chaines c2 = new Chaines
            {
                NomChaine = "c2Nom",
                Description = "Chaine de c2",
                Categorie = Categories.sport,
                tags = "best, sport, funny",
                Membres = m2,
                VideosChaine = null
            };
            context.Chaines.Add(c2);
            Chaines c3 = new Chaines
            {
                NomChaine = "c3Nom",
                Description = "Chaine de c3",
                Categorie = Categories.cuisine,
                tags = "food",
                Membres = m3,
                VideosChaine = null
            };
            context.Chaines.Add(c3);
            Chaines c4 = new Chaines
            {
                NomChaine = "c4Nom",
                Description = "Chaine de c4",
                Categorie = Categories.art,
                tags = "beauty, makeup, painting",
                Membres = m4,
                VideosChaine = null
            };
            context.Chaines.Add(c4);
            Chaines c5 = new Chaines
            {
                NomChaine = "c5Nom",
                Description = "Chaine de c5",
                Categorie = Categories.comedie,
                tags = "funny, drôle",
                Membres = m5,
                VideosChaine = null
            };
            context.Chaines.Add(c5);
            //-----------------------------------------------------------------

            Video v1 = new Video
            {
                titreVideo = "v1Nom",
                CompteurVue = 0,
                DateParution = new DateTime(2017,1,18),
                Categorie = Categories.jeux,
                Description = "",
                tags = "gaming",
                Chaines = c1,
                srcImg = "~/Images/f1.png",
                srcVid = "~/Videos/test.mp4"


            };
            context.Video.Add(v1);

            Video v2 = new Video
            {
                titreVideo = "v2Nom",
                CompteurVue = 0,
                DateParution = new DateTime(2018, 3, 18),
                Categorie = Categories.sport,
                Description = "Video de v2",
                tags = "",
                Chaines = c2,
                srcImg = "~/Images/f2.png",
                srcVid = "~/Videos/jellyfish.mp4"


            };
            context.Video.Add(v2);

            Video v3 = new Video
            {
                titreVideo = "v3Nom",
                CompteurVue = 0,
                DateParution = new DateTime(2015, 9, 8),
                Categorie = Categories.cuisine,
                Description = "Video de v3",
                tags = "food",
                Chaines = c3,
                srcImg = "~/Images/f3.png",
                srcVid = "~/Videos/lego.mp4"


            };
            context.Video.Add(v3);

            Video v4 = new Video
            {
                titreVideo = "v4Nom",
                CompteurVue = 0,
                DateParution = new DateTime(2011, 11, 1),
                Categorie = Categories.musique,
                Description = "Video de v4",
                tags = "musique, music",
                Chaines = c4,
                srcImg = "~/Images/f4.png",
                srcVid = "~/Videos/star_trails.mp4"


            };
            context.Video.Add(v4);

            Video v5 = new Video
            {
                titreVideo = "v5Nom",
                CompteurVue = 0,
                DateParution = new DateTime(2014, 6, 6),
                Categorie = Categories.comedie,
                Description = "Video de v5",
                tags = "funny, video, 2019",
                Chaines = c5,
                srcImg = "~/Images/f5.png",
                srcVid = "~/Videos/wierd.mp4"


            };
            context.Video.Add(v5);
            context.SaveChanges();
        }
        private string EncrypteMotDePasse(string password)
        {
            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            string encodedPasswordSentToFOrm = BitConverter.ToString(hash);

            return encodedPasswordSentToFOrm;
        }

    }

}