﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;

namespace Youtube.Models.DataModels
{
    [Table("Chaines")]
    public class Chaines
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PKChainesId { get; set; }
        [DisplayName("Nom de la chaine")]
        [Index(IsUnique = true), Required, MaxLength(30), MinLength(5)]
        public String NomChaine { get; set; }
        [Required, RegularExpression("^\\s*(\\S+\\s+|\\S+$){3,100}$", ErrorMessage = "Il faut au moins 3 mots et moins que 100 mots"), MaxLength(700)]
        public String Description { get; set; }

        [DisplayName("Catégorie")]
        [Required, EnumDataType(typeof(Categories))]
        public Categories Categorie { get; set; }
        [DisplayName("Tags")]
        [MaxLength(120)]
        public String tags { get; set; }
        [ForeignKey("Membres")]
        public int FKMembresId { get; set; }
        [InverseProperty("Chaines")]
        [ForeignKey("FKMembresId")]
        public virtual Membres Membres { get; set; }

        [InverseProperty("Chaines")]
        public virtual List<Video> VideosChaine { get; set; }

        public long vus { get; set; }

    }
}