﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Youtube.Models.DataModels;

using YoutubeTP.Models.ViewModels;

namespace YoutubeTP.Controllers
{
    [Authorize]
    public class MembresController : Controller
    {
        private YoutubeDB db = new YoutubeDB();

        

        // GET: Membres/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Membres membres = db.Membres.Find(id);
            if (membres == null)
            {
                return HttpNotFound();
            }
            return View(membres);
        }

       


        // GET: Membres/Edit/5
        public ActionResult Edit()
        {
            int MembresID = Convert.ToInt32(User.Identity.Name);
            Membres m = db.Membres.Find(MembresID);
            Profil p = new Profil
            {
                Utilisateur = m.NomUtilisateur,
                Sexe = m.Sexe,
                Courriel = m.Courriel,
                DateNaissance = m.DateNaissance
            };

            return this.View(p);
        }

        // POST: Membres/Edit/5
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Profil p)
        {
            if (ModelState.IsValid)
            {
                //Crée un hash pour que le mdp soit inlissible sur la bd
                string MotDePasseEncrypte = EncrypteMotDePasse(p.MotDePasse);
                int MembresID = Convert.ToInt32(User.Identity.Name);
                Membres membre = db.Membres.Find(MembresID);
                
                    membre.NomUtilisateur = p.Utilisateur;
                membre.Courriel = p.Courriel;
                membre.HashMotDePasse = MotDePasseEncrypte;
                membre.DateNaissance = p.DateNaissance;
                membre.Sexe = p.Sexe;
                
                db.Entry(membre).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View(p);
            }
        }

        // GET: Membres/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Membres membres = db.Membres.Find(id);
            if (membres == null)
            {
                return HttpNotFound();
            }
            if (membres.PKMembreId != Convert.ToInt32(User.Identity.Name))
            {
                return Content("Acces denied");
            }
            return View(membres);
        }

        // POST: Membres/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Membres membres = db.Membres.Find(id);
            db.Membres.Remove(membres);
            db.SaveChanges();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index","Home");
        }
        //historique
        public ActionResult Historique(int id)
        {
            Membres membres = db.Membres.Find(id);
            ViewBag.hist = membres.HistoriqueVideos;
            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        private string EncrypteMotDePasse(string password)
        {
            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            string encodedPasswordSentToFOrm = BitConverter.ToString(hash);

            return encodedPasswordSentToFOrm;
        }
    }
    
}
