﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Youtube.Models.DataModels;

namespace YoutubeTP.Controllers
{
    [Authorize]
    public class VideosController : Controller
    {
        private YoutubeDB db = new YoutubeDB();
        // GET: Videos
        public ActionResult Index()
        {
            var video = db.Video.Include(v => v.Chaines);
            return View(video.ToList());
        }

        // GET: Videos/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Video video = db.Video.Find(id);
            if (video == null)
            {
                return HttpNotFound();
            }
            else
            {
                video.CompteurVue++;
                
                //si user connecter, le trouve et fait historique
                if (User.Identity.IsAuthenticated)
                {
                    int MembresID = Convert.ToInt32(User.Identity.Name);
                    Membres m = db.Membres.Find(MembresID);
                    m.HistoriqueVideos.Add(video);
                }
                db.SaveChanges();
            }
            return View(video);
        }

        // GET: Videos/Create
        public ActionResult Create()
        {
            return View();
        }


        // POST: Videos/Create
        // Afin de déjouer les attaques par sur-validation, activez les propriétés spécifiques que vous voulez lier. Pour 
        // plus de détails, voir  https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase fichier)
        {
            //string relPath="";
            if (fichier != null)
            {
                try
                {
                    string nomComplet = "";
                    if (fichier.ContentType.Contains("image"))
                    {
                        nomComplet = Server.MapPath("~/Images/" + fichier.FileName);
                        fichier.SaveAs(nomComplet);
                        //utilise TempData pour garder la valeur
                        TempData["srcImg"] = "~/Images/" + fichier.FileName;
                        TempData["uploadImg"] = "Succes du televersement de" + fichier.FileName;
                    }
                    if (fichier.ContentType.Contains("video"))
                    {
                        //enregistre fichier mp4 dans le repo source
                        //relPath = "~/Videos/" + fichier.FileName;
                        nomComplet = Server.MapPath("~/Videos/" + fichier.FileName);
                        fichier.SaveAs(nomComplet);
                        //utilise TempData pour garder la valeur
                        TempData["srcVid"] = "~/Videos/" + fichier.FileName;
                        TempData["uploadVid"] = "Succes du televersement de" + fichier.FileName;
                    }

                    


                    return RedirectToAction("Create", "Videos");
                }
                catch (Exception e)
                {
                    TempData["upload"] = "Echec du televersement";
                    TempData["srcVid"] = "";
                    TempData["srcImg"] = "";
                    return RedirectToAction("Create", "Videos");

                }


            }
            else
            {
                TempData["srcVid"] = "";
                TempData["srcImg"] = "";
                TempData["upload"] = "Echec du televersement";
                return RedirectToAction("Create", "Videos");
            }
                
            
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Video video)
        {

            if (ModelState.IsValid && TempData["srcVid"]!=null && TempData["srcImg"] != null)
            {
                video.Chaines = db.Chaines.Find(video.FKChainesId);
                //reste a uploadé la miniature
                Video v = new Video
                {
                    CompteurVue = 0,
                    DateParution = DateTime.Today,
                    srcVid = (string)TempData["srcVid"],
                    srcImg = (string)TempData["srcImg"],
                    Categorie = video.Categorie,
                    Description = video.Description,
                    tags = video.tags,
                    titreVideo = video.titreVideo,
                    Chaines = video.Chaines
                };
                
                db.Video.Add(v);
                db.SaveChanges();
                return RedirectToAction("Details","Videos", new {id = v.PKVideoId });
            }

            return View(video);
        }

       

        // GET: Videos/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Video video = db.Video.Find(id);
            if (video == null)
            {
                return HttpNotFound();
            }
            if (video.Chaines.FKMembresId != Convert.ToInt32(User.Identity.Name))
            {
                return Content("Acces denied");
            }
            return View(video);
        }

        // POST: Videos/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Video video = db.Video.Find(id);
            db.Video.Remove(video);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
