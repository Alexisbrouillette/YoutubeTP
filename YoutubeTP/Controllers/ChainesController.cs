﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Youtube.Models.DataModels;

namespace YoutubeTP.Controllers
{
    [Authorize]
    public class ChainesController : Controller
    {
        private YoutubeDB db = new YoutubeDB();

       

        // GET: Chaines/Details/5
        [AllowAnonymous]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chaines chaines = db.Chaines.Find(id);
            if (chaines == null)
            {
                return HttpNotFound();
            }
            return View(chaines);
        }

        // GET: Chaines/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Chaines/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Chaines chaine)
        {
            

            if (ModelState.IsValid)
            {
                int mPkID = Convert.ToInt32(User.Identity.Name);
                Membres m = db.Membres.First(m1 => m1.PKMembreId == mPkID);
                chaine.Membres = m;
                Chaines chaines = new Chaines
                {
                    Membres = m,
                    NomChaine = chaine.NomChaine,
                    Description = chaine.Description,
                    Categorie = chaine.Categorie,
                    tags = chaine.tags,
                    
                };
                db.Chaines.Add(chaines);
                db.SaveChanges();
                return RedirectToAction("Index","Home");
            }
            else
            {
                return View(chaine);
            }
        }

        // GET: Chaines/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Chaines chaines = db.Chaines.First(c1 => c1.PKChainesId == id);
            if (chaines == null)
            {
                return HttpNotFound();
            }
            if (chaines.FKMembresId != Convert.ToInt32(User.Identity.Name))
            {
                return Content("Acces denied");
            }
            
            return View(chaines);
        }

        // POST: Chaines/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit( Chaines chaine)
        {
            if (ModelState.IsValid)
            {
                int mPkID = Convert.ToInt32(User.Identity.Name);
                chaine.FKMembresId = mPkID;
                
                db.Entry(chaine).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Details",new { id = chaine.PKChainesId });
            }
            
            return View(chaine);
        }

        // GET: Chaines/Delete/5
        public ActionResult Delete(int? id)
        { 
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Chaines chaines = db.Chaines.Find(id);
            if (chaines == null)
            {
                return HttpNotFound();
            }
            if (chaines.FKMembresId != Convert.ToInt32(User.Identity.Name))
            {
                return Content("Acces denied");
            }
            return View(chaines);
        }

        // POST: Chaines/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Chaines chaines = db.Chaines.Find(id);
            db.Chaines.Remove(chaines);
            db.SaveChanges();
            return RedirectToAction("Details","Membres", new { id = Convert.ToInt32(User.Identity.Name) });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
