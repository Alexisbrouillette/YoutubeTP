﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Youtube.Models.DataModels;
using YoutubeTP.Models.ViewModels;

namespace YoutubeTP.Controllers
{
    public class AuthController : Controller
    {
        private YoutubeDB db = new YoutubeDB();
        

        [HttpGet]
        public ActionResult Inscription() /* pour afficher le formulaire de création de compte */
        {
            return View(new Inscription());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Inscription(Inscription inscrit) /* Pour effectuer la création du compte*/
        {
            try
            {
                if (ModelState.IsValid)
                {
                    //Crée un hash pour que le mdp soit inlissible sur la bd
                    string MotDePasseEncrypte = EncrypteMotDePasse(inscrit.MotDePasse);
                    Membres membre = new Membres
                    {
                        NomUtilisateur = inscrit.Utilisateur,
                        Courriel = inscrit.Courriel,
                        HashMotDePasse = MotDePasseEncrypte,
                        DateNaissance = inscrit.DateNaissance,
                        Sexe = inscrit.Sexe
                    };

                    db.Membres.Add(membre);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return View(inscrit);
                }
            }
            catch
            {
                return View();
            }
        }
        [HttpGet]
        public ActionResult LoginForm() /* Pour afficher le formulaire de login */
        {
            return View(new LoginForm());
        }
        [HttpPost]
        public ActionResult LoginForm(string NomUtilisateur, string MotDePasse, string ReturnUrl = "") /* Pour traiter la
demande de login */
        {
            string MotDePasseEncrypte = EncrypteMotDePasse(MotDePasse);

            Membres membre = db.Membres.FirstOrDefault(m => m.NomUtilisateur == NomUtilisateur && m.HashMotDePasse == MotDePasseEncrypte );
            
            ViewBag.ReturnUrl = ReturnUrl;
            if (membre == null)
            {
                ViewBag.Message = "Le compte n'existe pas ou le mot de passe est invalide!";
                return View();
            }
            else
            {
                FormsAuthentication.SetAuthCookie(membre.PKMembreId.ToString(), false);
                if (ReturnUrl == "")
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    return Redirect(ReturnUrl);
                }
            }
        }
        [Authorize]
        public ActionResult Logout() /* Pour déconnecter l'utilisateur */
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        private string EncrypteMotDePasse(string password)
        {
            byte[] encodedPassword = new UTF8Encoding().GetBytes(password);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            string encodedPasswordSentToFOrm = BitConverter.ToString(hash);

            return encodedPasswordSentToFOrm;
        }
    }
}
