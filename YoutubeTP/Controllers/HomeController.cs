﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Youtube.Models.DataModels;

namespace YoutubeTP.Controllers
{
    public class HomeController : Controller
    {
        private YoutubeDB db = new YoutubeDB();
        //chiane pour retourner
        List<Chaines> chaineRecherche = new List<Chaines>();
        List<Video> videoRecherche = new List<Video>();
        //chaine filtre par categorie pour comparer
        List<Chaines> lC = new List<Chaines>();
        List<Video> lV = new List<Video>();
        //tableau des categories
        List<string> ARCategories =new List<string>(Enum.GetNames(typeof(Categories)));
        //tableau des filtres
        List<string>lFiltres=new List<string> { "Aucun","Popularite", "Date de mise en ligne" };
    // GET: Chaines

    public ActionResult Index()
        {

            ViewBag.categories = ARCategories;
            ViewBag.Chaines = this.GetChaines();
            ViewBag.Videos = this.GetVideos();
            return View();
        }

        //RECHERCHE
        [HttpPost]
        public ActionResult Recherche(FormCollection formValue)
        {
            videoRecherche = GetVideos();
            chaineRecherche = GetChaines();
            string search, cat;
            //si categorie est null
            if (formValue["categorie"] != "tout")
            {
                cat = formValue["categorie"].ToString();
                videoRecherche = db.Video.Where(e => e.Categorie.ToString().Equals(cat)).ToList();
                chaineRecherche = db.Chaines.Where(e => e.Categorie.ToString().Equals(cat)).ToList();
                //met la categorie selctionne en premier pour affichange dans le select
                ARCategories= swap(ARCategories, cat);
            }

            //si search est null
            if (formValue["search"] != "")
            {
                //stocker la valeur du search formulaire dans un string
                search = formValue["search"].ToString();
                //appel de la methode
                recherche(search);

            }

            //classer resultats selon pop ou mel
            if (formValue["filtre"] != "aucun" && formValue["filtre"] !=null)
            {

                string vFiltre = formValue["filtre"].ToString();
                lFiltres = swap(lFiltres, vFiltre);
                if (vFiltre == "Popularite")
                {

                    videoRecherche = videoRecherche.OrderByDescending(x => x.CompteurVue).ToList();
                    calcPopChaine();
                }
                if (vFiltre == "Mise en ligne")
                {
                    videoRecherche = videoRecherche.OrderByDescending(x => x.DateParution).ToList();
                }
            }
            ViewBag.Chaines = chaineRecherche;
            ViewBag.Videos = videoRecherche;
            ViewBag.categories = ARCategories;
            ViewBag.filtres = lFiltres;
            
            return View();
        }



        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }








        //methodes ------------------------------------------------------------

        //swap item dans une liste
        public List<string> swap(List<string>l, string x)
        {
            int index =l.IndexOf(x);
            string temp = l[0].ToString();
            l[0] = x;
            l[index] = temp;
            return (l);
        }
        //popularite dune chaine
        public void calcPopChaine()
        {
            
            //met dans vus(var) de vue le nombre de vus par chaines
            foreach (Chaines x in chaineRecherche)
            {
                foreach (Video a in x.VideosChaine)
                {
                    x.vus += (int)a.CompteurVue;
                }
                
            }
            //ordoner le tableau selon les vue
            //classe les chaines selon leur vus
            chaineRecherche=chaineRecherche.OrderByDescending(x => x.vus).ToList();

        }
        //retourne des list pour afficher contenu
        public List<Chaines> GetChaines()
        {
            var chaines = db.Chaines;

            return (chaines.ToList());
        }



        public List<Video> GetVideos()
        {
            var videos = db.Video;
            List<Video> lVideos = videos.ToList();
            //sort la liste de video selon le nombre de vus
            return (lVideos);
        }





        public void recherche(string mots)
        {
            string[] motsCles = mots.Split(null);
            //listes temporaires de vid et chaines selon categorie
            //copie des liste dans les temporaires
            lC = new List<Chaines>(chaineRecherche);
            lV = new List<Video>(videoRecherche);
            //recherche selon le premier mot cle
            //recherche selon nom 
            chaineRecherche = chaineRecherche.Where(e => e.NomChaine.Contains(motsCles[0])).ToList();
            videoRecherche = videoRecherche.Where(e => e.titreVideo.Contains(motsCles[0])).ToList();
            //recherche selon description et evite les doublons
            chaineRecherche.AddRange(chaineRecherche.Where(e => e.Description.Contains(motsCles[0]) && !chaineRecherche.Contains(e)).ToList());
            videoRecherche.AddRange(videoRecherche.Where(e => e.Description.Contains(motsCles[0]) && !videoRecherche.Contains(e)).ToList());
            //si plus dun mot cle on doit jaouter a la premier recherche pour pas perde des resultats
            if (motsCles.Count() > 1)
            {
                //boucle pour cherche avec tous les mots cles
                for (int i = 1; i < motsCles.Count(); i++)
                {
                    //recherche selon nom 
                    chaineRecherche.AddRange(lC.Where(e => e.NomChaine.Contains(motsCles[i])).ToList());
                    videoRecherche.AddRange(lV.Where(e => e.titreVideo.Contains(motsCles[i])).ToList());
                    //recherche selon description
                    chaineRecherche.AddRange(lC.Where(e => e.Description.Contains(motsCles[i]) && !chaineRecherche.Contains(e)).ToList());
                    videoRecherche.AddRange(lV.Where(e => e.Description.Contains(motsCles[i]) && !videoRecherche.Contains(e)).ToList());
                }
            }

        }

    }
}